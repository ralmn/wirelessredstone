package fr.ralmn.wirelessredstone;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author ralmn
 */
public class WirelessRedStone extends JavaPlugin{

    private static WirelessRedStone instance;
    public ArrayList<WirelessControler> controllers = new ArrayList<WirelessControler>();

    public static WirelessRedStone getInstance(){
        return instance;
    }

    @Override
    public void onDisable() {
    }

    @Override
    public void onEnable() {
        getDataFolder().mkdirs();
        instance = this;
        getServer().getPluginManager().registerEvents(new SignListener(this), this);
        load();

    }

    public void load(){
        try{
            for (File file : getDataFolder().listFiles()) {
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);
                String raw = "", line;
                while ((line = br.readLine()) != null){
                    raw += line;
                }

                JSONObject jsonObject = new JSONObject(raw);
                String name = jsonObject.getString("name");
                Location l = Utils.stringToLocation(jsonObject.getString("input"));
                if(!(l.getBlock().getState() instanceof Sign)) continue;

                WirelessControler controler = new WirelessControler(name,l.getBlock());

                System.out.println("LOADED : " + controler.getName());

                JSONArray array = jsonObject.getJSONArray("output");
                for(int i = 0; i < array.length(); i++){
                    String s = array.getString(i);
                    l = Utils.stringToLocation(s);
                    if(!(l.getBlock().getState() instanceof Sign)){
                        continue;
                    }

                    controler.addOuput(l.getBlock());
                }
                controler.save();
                controllers.add(controler);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public WirelessControler getControler(String name) {

        for (WirelessControler controller : controllers) {
            if(controller.getName().equalsIgnoreCase(name)){
                return controller;
            }
        }
        return null;
    }
}
