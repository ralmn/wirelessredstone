package fr.ralmn.wirelessredstone;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.SignChangeEvent;

/**
 * @author ralmn
 */
public class SignListener implements Listener {

    private final WirelessRedStone plugin;

    public SignListener(WirelessRedStone plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onSignPlace(final SignChangeEvent event) {
        if (event.getLine(0).equalsIgnoreCase("wr")) {
            if (event.getLine(1).startsWith("in")) {
                WirelessControler controler = new WirelessControler(event.getLine(2), event.getBlock());
                plugin.controllers.add(controler);
                boolean input  =controler.readRedstoneInputAndUpdateOutput();
                event.setLine(0, ChatColor.GREEN + "WirelessRedStone");
                event.setLine(1, ChatColor.AQUA + "INPUT");
                event.setLine(2, ChatColor.GOLD + controler.getName());
                event.setLine(3, ChatColor.DARK_RED + (input ? "ON" : "OFF"));
            } else {
                WirelessControler controler = plugin.getControler(event.getLine(2));
                if (controler != null) {
                    controler.addOuput(event.getBlock());
                    boolean input  =controler.readRedstoneInputAndUpdateOutput();
                    event.setLine(0, ChatColor.GREEN + "WirelessRedStone");
                    event.setLine(1, ChatColor.AQUA + "OUTPUT");
                    event.setLine(2, ChatColor.GOLD + controler.getName());
                    event.setLine(3, ChatColor.DARK_RED + (input ? "ON" : "OFF"));
                    controler.save();

                } else {
                    event.getBlock().breakNaturally();
                }
            }
        }
    }

    @EventHandler
    public void onRedstoneStateChange(BlockRedstoneEvent e) {

        if(e.getOldCurrent() == e.getNewCurrent()){
            for (WirelessControler controller : plugin.controllers) {
                controller.readRedstoneInputAndUpdateOutput();
            }
        }
        e.getBlock().getState().update(true,true);
    }

}
