package fr.ralmn.wirelessredstone;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_6_R3.block.CraftBlock;
import org.bukkit.material.Lever;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author ralmn
 */
public class WirelessControler {

    private final String name;
    private final ArrayList<Block> signsOuput = new ArrayList<Block>();
    private final Block signInput;


    public WirelessControler(String name, Block signInput) {
        this.name = name;
        this.signInput = signInput;
    }

    public void setRedstoneOuput(boolean output) {
        for (Block block : signsOuput) {
            if (block.getState() instanceof Sign) {
                Sign s = (Sign) block.getState();
                s.setLine(3, ChatColor.DARK_RED + (output ? "ON" : "OFF"));
                s.update();
            }
            for (BlockFace blockFace : BlockFace.values()) {
                Block face = ((CraftBlock) block).getFace(blockFace);
                if (face.getType() == Material.LEVER) {
                    BlockState state = face.getState();
                    Lever lever = (Lever) state.getData();
                    lever.setPowered(output);
                    state.setData(lever);
                    face.setData(lever.getData(),true);
                    state.update(true,true);
                }

            }
        }
    }

    public boolean readRedstoneInputAndUpdateOutput() {
        boolean input = false;
        for (BlockFace blockFace : BlockFace.values()) {
            Block face = ((CraftBlock) signInput).getFace(blockFace);
            if (face.isBlockPowered()) {
                input = true;
                continue;
            }
        }
        if (signInput.getState() instanceof Sign) {
            Sign s = (Sign) signInput.getState();
            s.setLine(3, ChatColor.DARK_RED + (input ? "ON" : "OFF"));
            s.update();
        }
        setRedstoneOuput(input);
        return input;
    }

    public void save() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("input", Utils.locationToString(signInput.getLocation()));
        JSONArray outputs = new JSONArray();
        for (Block block : signsOuput) {
            outputs.put(Utils.locationToString(block.getLocation()));
        }
        jsonObject.put("output", outputs);
        jsonObject.put("name", name);
        try {
            File f = new File(WirelessRedStone.getInstance().getDataFolder(), name + ".json");
            if (!f.exists()) f.createNewFile();
            FileWriter fw = new FileWriter(f, false);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(jsonObject.toString());
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void addOuput(Block block) {
        if (!containsOutput(block))
            this.signsOuput.add(block);
    }

    public void delOuput(Block block) {
        if (containsOutput(block))
            this.signsOuput.remove(block);
    }

    public boolean containsOutput(Block face) {
        return this.signsOuput.contains(face);
    }

    public boolean isInput(Block face) {
        return signInput == face;
    }
}
