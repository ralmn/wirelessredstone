package fr.ralmn.wirelessredstone;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * @author ralmn
 */
public class Utils {

    public static Location stringToLocation(String s) {
        if (s == null) return null;
        String[] split = s.split(";");

        double x = Double.parseDouble(split[0]);
        double y = Double.parseDouble(split[1]);
        double z = Double.parseDouble(split[2]);
        World world = Bukkit.getWorld(split[3]);
        return new Location(world, x, y, z);
    }

    public static String locationToString(Location l) {
        return l.getX() + ";" + l.getY() + ";" + l.getZ() + ";" + l.getWorld().getName();
    }

}
